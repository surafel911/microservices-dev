# Data at the Point of Care

Sample project using microservices to mimic the [Data at the Point of Care](https://dpc.cms.gov/) service from the [Centers for Medicare & Medicaid Services](https://www.cms.gov/).

There are two versions of the app. One written in .NET Core 3.1 (serach for the [c#-dotnet-core-3.1](https://gitlab.com/surafel911/microservices-dev/-/tree/c%23-dotnet-core-3.1?ref_type=heads) branch) and this main branch using Go.
